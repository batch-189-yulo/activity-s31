// 1. What directive is used by Node.js in loading the modules it needs?
//ans: 1. Require directive


// 2. What Node.js module contains a method for server creation?
//ans: 2. http

// 3. What is the method of the http object responsible for creating a server using Node.js?
// ans: 3. createServer()


// 4. What method of the response object allows us to set status codes and content types?
// ans: 4. writeHead()


// 5. Where will console.log() output its contents when run in Node.js?
// ans: 5. terminal


// 6. What property of the request object contains the address's endpoint?
// ans: 6. URL
